import java.io.Closeable;
import java.io.FileNotFoundException;
import java.io.IOException;

public class Mouth implements AutoCloseable{

	@Override
	// if Mouth implements Closable, then close can only throws IOException.
	// public void close() throws NotHungryException{}      failed, because NotHungryException is not a subclass of IOException
	public void close() throws IOException, NotHungryException {   // not required to have throws exception, and I can throw any subclasses of Exception in here
		// I can make a method throws as many exceptions as I want, but I need to handle the exception properly
		System.out.println("mouth close method called");
		// even though close() declared to throw an IOException, it is not required to throw an IOException at the end of close() method.
		// however, I Still need to handle the exception after calling close() method. There is two way to do it:
		// 1. make the method that called close() method throws an exception
		// 2. make a catch statement after call close() method
		throw new NotHungryException("NotHungryException");
		// has to have throws exception here, because the method body throw an exception
	}
	public static void main(String arg[]) {   // 1. throws Exception, if I dont have the catch statement after try
		try(Mouth mouth = new Mouth()){
			System.out.println("mouth is open, eating foods");
		}//{  System.out.println("something between try and catch block is not allowed"); }
		// System.out.println("something between try and catch block is not allowed");
		catch(FileNotFoundException | NotHungryException e) {
			System.out.println("Exception occured:  "+e.getMessage());
		}catch(IOException e) {
			// catch IOException is required here, even though we catch the subclass (FileNotFoundException) of IOException in the previous catch. 
			// because you might throw other kind of IOException in the close() method.
			System.out.println("Ooops, other IOException occured! How is that possible?");
			assert false : "this block of code is impossible to reach, something is wrong!";
			// an assertion is a good idea here to make sure that this clock of code can never be reached.
		}
	}
}
