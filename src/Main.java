import java.io.IOException;

public class Main {
	// suppress exception practice: 
	
	public class ObjOne implements AutoCloseable{

		@Override
		public void close() throws ExceptionOne{
			throw new ExceptionOne("Exception One");
		}
	}
	public class ExceptionOne extends Exception{
		public ExceptionOne(String string) {
			super(string);
		}
	}
	public static void main(String args[]) {
		  try(ObjOne temp = new Main().new ObjOne()){
			throw new IOException("Primary exception: IOException");
		}catch(IOException e) {
			System.out.println("IOException caught!");   // primary exception
		}catch(Exception e) {      // required even though close method is suppressed
			System.out.println("Exception caught");
		}

		
		try(ObjOne temp = new Main().new ObjOne();
				ObjOne temp2 = new Main().new ObjOne()){
		}catch(ExceptionOne e) {
			System.out.println("caught exceptionOne");
			// in this case, exception from temp2 close() will be caught, temp exception is suppressed
		}
		
		try(ObjOne temp = new Main().new ObjOne()){
			
		}catch(Exception e) {
			System.out.println("ExceptionOne caught");
			// in this case, temp exception from close() method is the primary exception, so it has been caught by catch statement 
			// throw e; yes, you can re-throw an exception, if doing so, main need to have throws Exception !
		}
	}
}
