
public class NotHungryException extends Exception{
	// nothing is required for this NotHungryException, I can just leave the class body blank
	public NotHungryException() {    // default constructor
		super();
	}
	public NotHungryException(Exception e) {   // constructor wraps another exception
		super(e);
	}
	public NotHungryException(String message) {   // constructor with own exception message
		super(message);
	}
	public static void main(String arg[]) {
		NotHungryException temp = new NotHungryException("this is a NotHungryException");
		try {
			throw temp;
		}//catch(NotHungryException | Exception e) { }
		// not compiling because NotHungryException is a Exception, already caught by Exception.
		/*catch(Exception e) {}
		catch(NotHungryException) {}*/ 
		// not working, because exceptions will be caught by the first catch(Exception e), catch(NotHungryException) will never be reached.
		// finally {}, not working, cause traditional try statement requires at least one catch statement to handle the exception
		catch(NotHungryException e) {
			System.out.println("catched an exception:  "+e.getMessage());
		}
		catch(Exception e) {
			System.out.println("unknown exception occured");
		}
		finally {
			System.out.println("the fianlly statement will always gets executed after catch statement, try->catch->finally");
		}
		
	}
}
